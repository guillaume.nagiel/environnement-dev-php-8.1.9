# Environnement de Développement SYMFONY

## Description du projet

### Introduction
Ce dépot va vous permettre de déployer un environnement de développement en `Symfony`. Une fois démarré, vous pourrez créer un projet `Symfony 6`

### Contenu
Dans ce container, vous trouverez des images de :
- [PHP 8.3.7](https://hub.docker.com/_/php).
- [MySql](https://hub.docker.com/_/mysql).
- [PhpMyAdmin](https://hub.docker.com/_/phpmyadmin).
- [MailDev](https://hub.docker.com/r/maildev/maildev).

## Prérequis
Pour utiliser ce container de manière simple, télécharger et lancer le logiciel [Docker Desktop](https://docs.docker.com/get-docker/)

## Installation

### Installation du container
Une fois `Docker Desktop` installé, vous pouvez cloner ce dépot.
```
git clone https://gitlab.com/guillaume.nagiel/symfony_env.git
```

Puis ouvrez ce dossier avec `VSCode`.

Nous allons maintenant ouvrir cette image docker en tant que `Conteneur de développement` dans `VSCode`:
- Accédez à la palette de commandes en appuyant sur Ctrl+Shift+P (ou Cmd+Shift+P sur macOS).
- Saisissez `Dev Containers: Open Folder in Container`.
- S'il vous est demandé un fichier de configuration, selectionnez `SYMFONY Dev Container`.

Le conteneur devrait s'installer avec les extentions `VSCode` pour `PHP`.

### Vérification des services lancés
Nous sommes maintenant positionné dans le dossier `/var/www` du container. 

Pour vérifier la version de `PHP`, et par la même occasion, voir si le serveur `Apache` est bien lancé, tapez
```
php -v
```
Vous devriez avoir la version 8.1.9 de `PHP`

Vous allez ensuite tester la version de `composer`
```
composer -V
```
Vous devriez avoir la version 2.4.0 de `composer`

Il ne reste plus qu'a tester le serveur `MySQL` et `PhpMyAdmin`. Pour cela, dans votre navigateur, rendez vous à l'adresse [http://localhost:8080/](http://localhost:8080/) et saisissez `root` pour l'utilisateur sans mot de passe. Si vous arrivez à vous connecter à `PhpMyAdmin`, c'est que le serveur `MySQL` est bien lancé.
Une autre solution consiste à lancer cette page directement depuis `Docker Desktop`. Dans le container `phpmyadmin_docker_symfony`, cliquez sur `OPEN WITH BROWSER`.

### Création d'un projet Symfony 6
Pour créer un projet `Symfony 6` dans cette environnement, lancer une commande `composer` depuis le container
```
composer create-project symfony/website-skeleton project
```
> :warning: **Si vous voulez changer le nom du projet ou installer plusieurs projets**: Il vous faudra modifier le fichier [vhosts.conf](/php/vhosts/vhosts.conf).

Tapez
```
cd project
```
Vous voilà maintenant dans votre projet `Symfony`, le serveur étant déjà lancé, il ne vous reste plus qu'a aller à l'adresse [http://localhost:8741/](http://localhost:8741/) pour arriver sur la page d'accueil de votre projet.

Bon développement :sunglasses:

# Liste des fonctionnalités à ajouter

### Todo

### In Progress

### Done ✓

- [x] Créer le README.md  
- [x] Créer le TODO.md  
- [x] Ajouter l'installation de `Symfony CLI` au fichier [Dockerfile](/php/Dockerfile).  
  - [x] Mettre à jour le README.md  